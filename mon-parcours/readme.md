---
mermaid: true
---

# Mon parcours Devoirs Faits

Bienvenue sur la page "Mon parcours Devoirs Faits" qui a pour but d'accompagner les élèves dans le dispositif "devoirs faits" ou dans leur travail autonome. Cette interface interactive guide l'élève à travers différentes étapes pour réaliser et vérifier ses devoirs.

## Une version en ligne et une appli androïd

- La version en ligne est disponible [ici🖱️](https://clg_v_vasarely.forge.apps.education.fr/devoirs-faits/mon-parcours/)

- L'application Androïd (APK) est téléchargeable [ici🖱️](https://forge.apps.education.fr/clg_v_vasarely/devoirs-faits/-/blob/main/mon-parcours/mon-parcours.apk)

## Fonctionnalités

- **Navigation interactive** : La page utilise une série de sections interactives qui permettent à l'élève de choisir entre différents parcours et de le guider étape après étape dans son travail.
- **Suivi de progression** : Des barres de progression visuelles montrent l'avancement dans le travail ou les révisions.
- **Ressources et liens** : Accès direct à des outils externes comme Pronote, et des liens vers des ressources pour réviser ou compléter des devoirs.
- **Évaluation et feedback** : À la fin de la session, l'élève peut évaluer sa séance et laisser un feedback qui peut être utilisé pour améliorer les prochaines sessions.

## Structure

La page est structurée autour de plusieurs grands axes :

- **Accueil** : Introduction et choix initial entre travailler en autonomie ou lors d'une séance de devoirs faits.
- **Vérification des devoirs** : Options pour consulter l'agenda, vérifier les devoirs sur Pronote, ou confirmer que tous les devoirs sont réalisés.
- **Sélection du travail** : Après vérification, choix du type de travail à réaliser (exercices, préparation d'évaluation, révision, préparer un oral).
- **Assistance** : Options pour obtenir de l'aide, consulter des ressources ou communiquer avec un enseignant.
- **Méthodes de révision** : Conseils et stratégies pour organiser les révisions de manière efficace.
- **Activités supplémentaires** : Suggestions d'activités pour ceux ayant terminé leurs devoirs.
- **Évaluation de la séance** : Auto-évaluation de la séance de travail pour encourager la réflexion personnelle sur les méthodes d'apprentissage.

## Arborescence

```mermaid
graph TD;
    A(Choix 1 <br> Où je réalise mon travail) --> B[Devoirs faits]
    A --> C[En autonomie]
    
    B --> D(Choix 2 <br> Vérifier son travail)
    C --> D
    
    D --> E[Je consulte mon agenda]
    D --> A2[Je me mets jour]
    D --> F[Avec Pronote]
    D --> G[J'ai tout fini]
    
    E --> H(Choix 3 <br> Sélectionner un travail)
    F --> H
    G ----------> R(Choix 3' <br> Choisir d'autres activités)
    
    R --> S[Je crée <br> des exercices]
    R --> T[Je fais des <br> exercices en ligne]
    R --> U[Je choisis un <br> livre et je lis]
    
    H --> I[Je fais des <br> exercices]
    H --> J[Je prépare <br> une éval]
    H --> K[Je révise]
    H --> X[Je prépare <br> un oral]
    
    X ----> Y[Discours <br> et temps]
    X ----> Z[Conception <br> du support]
    X ----> Z2[Questions <br> du jury]
    
    K --> M[Je consulte des méthodes <br> pour m'aider à réviser]
    
    I ------> L(Choix 4 <br> J'ai besoin d'aide)
    
    L --> V[Je consulte mon cours]
    L --> N[Je demande de l'aide à <br> un intervenant ou à <br> un camarade disponible]
    L --> O[J'écris à mon prof]
    L --> B2[Je réalise une <br>recherche sur le web]

    A --> P{LOGO} 
    P --------------> Q(Auto-évaluation)
    Q --> W(Fin de la séance)

    style A fill:#f9f,stroke:#333,stroke-width:2px
    style D fill:#aee,stroke:#333,stroke-width:2px 
    style H fill:#cfc,stroke:#333,stroke-width:2px
    style L fill:#fc3,stroke:#333,stroke-width:2px  
    style R fill:#cfc,stroke:#333,stroke-width:2px
    style P fill:#ebb,stroke:#333,stroke-width:2px

```

## Technologies Utilisées

- HTML et CSS pour la structure et le style.
- JavaScript pour les interactions dynamiques et la gestion des événements.
- [FontAwesome](https://fontawesome.com/) pour les icones.


## Crédits et licence

### Crédits - Équipe "Devoirs-Faits" du Collège Victor Vasarely

Aurélie Lavanant (idée originale, arborescence et rédaction), Mathieu Moulierac (idée originale et arborescence) et Cyril Iaconelli (webdesign, arborescence et rédaction).


### Licence

Ce projet est disponible sous la licence [CC-BY](https://creativecommons.org/licenses/by/4.0/), permettant de partager et d'adapter le contenu tant que l'_Équipe "Devoirs-Faits" du Collège Victor Vasarely_ est créditée.
