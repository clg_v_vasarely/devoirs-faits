# Projet Devoirs Faits- Collège Victor Vasarely

Bienvenue sur la page du projet "Devoirs Faits" du Collège Victor Vasarely, situé à Le Mené, Côtes-d'Armor. Ce projet vise à partager des ressources éducatives destinées aux élèves pour les aider dans leurs devoirs après les cours et ainsi qu'aux intervenants en charge dans le cadre du dispositif national "devoirs faits".

## Objectifs

Le projet "Devoirs Faits" a pour objectif de :
- **Soutenir les élèves** dans la réalisation de leurs devoirs.
- **Fournir des ressources** accessibles pour faciliter l'apprentissage autonome.
- **Encourager la collaboration** entre élèves et enseignants.
- **Promouvoir l'égalité des chances** en éducation.

## Ressources Disponibles

Les ressources disponibles incluent :
- Guides d'étude et fiches récapitulatives.
- Liens vers des ressources externes recommandées.

## Licence

Toutes les ressources partagées dans le cadre du projet "Devoirs Faits" sont sous licence [Creative Commons Attribution (CC-BY)](https://creativecommons.org/licenses/by/4.0/). Cette licence permet à quiconque d'utiliser, adapter et redistribuer les matériaux, tant que l'équipe "Devoirs-Faits" du Collège Victor Vasarely est créditée.

## Contact

Pour plus d'informations sur ce projet, veuillez créer un ticket ! ;)

